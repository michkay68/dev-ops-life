# Dev-Ops-Life



## PROJECT OVERVIEW

Deploy a Node.js Serverless Web application that will host a static site. The Node.js app will be containerized with Docker and deployed into a container service. In this case, Amazon ECS. The fargate Launch type will be leveraged to minimalize additional configurations. The Node,js app will also have a MongoDB database, where the information will live. 
The static site will be a simple "Intro to Docker" static web page. 

## Resources 
(https://levelup.gitconnected.com/aws-fargate-running-a-serverless-node-js-app-on-aws-ecs-c5d8dea0a85a) 
(https://github.com/austinloveless/Docker-on-AWS) 
(https://medium.com/zenofai/how-to-build-a-node-js-and-mongodb-application-with-docker-containers-15e535baabf5)


***

